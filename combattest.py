import Player

import sys
sys.path.append('../..')
from monsters import zombie
from monsters import skeleton
from monsters import rat
from monsters import landoctopus
from monsters import shekelton
from monsters import ceilingjiggly
from monsters import jackpizza

#The order is attack, health, gold dropped and speed
#skeleton = skeleton.Skeleton('Skeleton', 3, 7, 50, 2)
zombie = zombie.Zombie('Zombie', 2, 5, 30, 1)
rat = rat.Rat('Rat', 1, 2, 10, 3)
landoctopus = landoctopus.Landoctopus('Mutant Octopus', 5, 11, 80, 2)
shekelton = shekelton.Shekelton('Lord Shekelton', 6, 15, 400, 3)
ceilingjiggly = ceilingjiggly.Ceilingjiggly('Ceiling Jiggly', 3, 3, 10, 1)
jackpizza = jackpizza.Jackpizza('Jack Pizza', 7, 30, 0, 5)

def skeletondeath(money):
    if skeleton.get_health() <= 0:
        money += 50
        character.set_money(money)
        print('You defeated the monster!')

def zombiedeath(money):
    if zombie.get_health <= 0:
        money += 30
        character.set_money(money)
        print('You defeated the monster!')

def ratdeath(money):
    if rat.get_health <= 0:
        money += 10
        character.set_money(money)
        print('You defeated the monster!')

def landoctopusdeath(money):
    if landoctopus.get_health <= 0:
        money += 80
        character.set_money(money)
        print('You defeated the monster!')

def shekeltondeath(money):
    if shekelton.get_health <= 0:
        money += 400
        character.set_money(money)
        print('You defeated the monster!')
def ceilingjigglydeath(money):
    if ceilingjiggly.get_health <= 0:
        money += 10
        character.set_money(money)
        print('You defeated the monster!')
def jackpizzadeath():
    if jackpizza.get_health <= 0:
        print('insert some lore crap here kieran')

character = Player.Player('butt', 9, 20, 0, 3, 5)
f = 2
r = 2
money = 50
'''
if f == 2 and r == 2:
    monster = 'put a monster here'
'''
monster = skeleton.Skeleton('Skeleton', 3, 7, 50, 2)

def combat(money):

    while monster.get_health() > 0 and character.get_health() > 0:
        skeletonattack = monster.get_attack()
        skeletonhealth = int(monster.get_health())
        characterhealth = int(character.get_health())
        characterattack = int(character.get_attack())
        print('Oh no, a', monster.get_name(), 'is attacking you!')
        decision = input('What do you want to do? a. Attack with your sword b. Consume a health potion c. Take a defensive stance ')

        if decision == 'a':
            if monster.get_speed() > character.get_speed():
                characternewhealth = characterhealth - skeletonattack
                character.set_health(characternewhealth)
                skeletonnewhealth = skeletonhealth - characterattack
                monster.set_health(skeletonnewhealth)
                print('Your health is', character.get_health())
                print("The monster's health is", monster.get_health())

            elif character.get_speed() > monster.get_speed():
                skeletonnewhealth = skeletonhealth - characterattack
                monster.set_health(skeletonnewhealth)
                if monster.get_health() > 0:
                    money += 50
                    character.set_money(money)
                    print('You defeated the monster!')
                    print('You now have', character.get_money(), 'gold')
                    break
                else:
                    characternewhealth = characterhealth - skeletonattack
                    character.set_health(characternewhealth)
                    print('Your health is', character.get_health())
                    print("The monster's health is", monster.get_health())

        if decision == 'b':
            characterhealth = character.get_health() + 10
            character.set_health(characterhealth)
            newpotions = character.get_potions() - 1
            character.set_potions(newpotions)
            characternewhealth = characterhealth - skeletonattack
            character.set_health(characternewhealth)
            print('Your health is', character.get_health())
            print('You have', character.get_potions(),'health potions left')
            print('The monster’s health is', monster.get_health())

        if decision == 'c':
            characternewhealth = characterhealth - (skeletonattack / 3)
            character.set_health(characternewhealth)
            print('Your health is', character.get_health())
            print('The monster’s health is', monster.get_health())

        if monster.get_health() <= 0:
            money += 50
            character.set_money(money)
            print('You defeated the monster!')
            print('You now have', character.get_money(), 'gold')

combat(money)