__author__ = 'jhoeflich2017'


class Player:
    def __init__(self, name, attack, health, money, speed, potions):
        self.__name = name
        self.__health = health
        self.__money = money
        self.__attack = attack
        self.__speed = speed
        self.__potions = potions

    def get_name(self):
        return self.__name

    def get_health(self):
        return self.__health

    def get_money(self):
        return self.__money

    def get_attack(self):
        return self.__attack

    def get_speed(self):
        return self.__speed

    def get_potions(self):
        return self.__potions

    def set_name(self, name):
        self.__name = name

    def set_health(self, health):
        self.__health = health

    def set_money(self, money):
        self.__money = money

    def set_attack(self, attack):
        self.__attack = attack

    def set_speed(self, speed):
        self.__speed = speed

    def set_potions(self, potions):
        self.__potions = potions
