# Griffin Moran

from tkinter import *
import start

class Ball:
    def __init__(self, canvas, img, x1, y1) -> object:
        self.x1 = x1
        self.y1 = y1

        self.canvas = canvas
        self.ball = self.canvas.create_image(x1, y1, image=img)

        self.canvas.bind("<w>", self.up)
        self.canvas.bind("<s>", self.down)
        self.canvas.bind("<a>", self.left)
        self.canvas.bind("<d>", self.right)
        self.canvas.focus_set()

    def up(self, event):
        y = 0
        y -= 10
        self.canvas.move(self.ball, 0, y)

    def down(self, event):
        y = 0
        y += 10
        self.canvas.move(self.ball, 0, y)

    def left(self, event):
        x = 0
        x -= 10
        self.canvas.move(self.ball, x, 0)

    def right(self, event):
        x = 0
        x += 10
        self.canvas.move(self.ball, x, 0)


# Create Window
window = Tk()
window.title("CODE = WORKING")

# Setup the canvas with a background images
mapWidth = 1600
mapHeight = 900
map = PhotoImage(file="city.gif")
canvas = Canvas(window, width=mapWidth, height=mapHeight, bg="white")
canvas.create_image(mapWidth / 2, mapHeight / 2, image=map)

# Create the movable object
ballImg = PhotoImage(file="girl.gif")
ball = Ball(canvas, ballImg, 100, 100)

# Pack the canvas
canvas.pack()
window.attributes("-topmost", True)
window.mainloop()
